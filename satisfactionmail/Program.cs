﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using RestSharp;

namespace satisfactionmail
{
    class Program
    {
#if DEBUG
            public static readonly string ENV = ConfigurationManager.AppSettings["env_uat"];
            public static readonly string Domain = ConfigurationManager.AppSettings["domain_uat"];
            public static readonly string ConnectionStringData = @"Data Source=10.17.251.160;Initial Catalog=DBCDSData;User ID=central;Password=Cen@tral";

#else
            public static readonly string ENV = ConfigurationManager.AppSettings["env_prd"];
            public static readonly string Domain = ConfigurationManager.AppSettings["domain_prd"];
            public static readonly string ConnectionStringData = @"Data Source=10.17.220.55;Initial Catalog=DBCDSData;User ID=central;Password=Cen@tral";
#endif
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Start Program Send Mail Satisfaction");
                Console.WriteLine($"Env : {ENV}");
                Console.WriteLine($"Domain : {Domain}");
                Console.WriteLine($"DateTime run : {DateTime.Now}");
                Console.WriteLine($"Delay for start program 5 second");
                Thread.Sleep(5000);
                List<string> orderGuids = new List<string>();
                using (SqlConnection connect = new SqlConnection(ConnectionStringData))
                {
                    connect.Open();
                    using (SqlCommand command = connect.CreateCommand())
                    {
                        string sql = @"SELECT OrderGuid FROM TBOrder WHERE IsDownload = 1 and convert(date,PaymentDate,101) = convert(date,dateadd(day,-7,GETDATE()),101)";
                        //string sql = @"select o.OrderGuid,o.UserEmail
                        //                From (
                        //                    select Row_number() Over(partition by UserEmail Order By PaymentDate desc) as RowNum,*
                        //                    FROM TBOrder
                        //                    WHERE IsDownload = 1 
                        //                    and convert(date,PaymentDate,101) = convert(date,dateadd(day,-7,GETDATE()),101)
                        //                ) o
                        //                WHERE o.RowNum = 1";
                        command.CommandText = sql;
                        command.CommandType = CommandType.Text;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                orderGuids.Add((string)reader["OrderGuid"]);
                            }
                        }
                        SendComplete(orderGuids);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception : {e.ToString()}");
            }
            Console.WriteLine($"End Program");
            Thread.Sleep(5000);
        }

        static void SendComplete(List<string> orderGuids)
        {
            foreach (var orderGuid in orderGuids)
            {
                try
                {
                    Console.WriteLine($"OrderGuids : {orderGuid}");
                    Thread.Sleep(10000);
                    var resource = "api/SendMail.aspx";
                    var request = new RestRequest(resource, Method.POST);
                    request.AddParameter("OrderGuId", orderGuid);
                    request.AddParameter("EmailType", 7);
                    var response = new RestResponse();
                    RestClient client = new RestClient(Domain);
                    Task.Run(async () =>
                    {
                        response = await GetResponseContentAsync(client, request) as RestResponse;
                    }).Wait();
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Console.WriteLine($"OrderGuids : {orderGuid} StatusCode : {System.Net.HttpStatusCode.OK}");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception OrderGuId : {e.ToString()}");
                }
            }
        }



        public static RestResponse PostAsync(string resource, string authorization, string message)
        {
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", authorization);
            request.AddQueryParameter("message", message);
            var response = new RestResponse();
            RestClient client = new RestClient(resource);
            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();
            return response;
        }

        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response => {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }
    }
}
